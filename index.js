
// Parameters and Arguments

	// conside this function
	function printName(name="noname"){
		console.log("My name is " + name);
	}

	printName("Chris");
	printName();


	// Variables can also be passed as an argument
	let sampleVariable = "Edward";
	printName(sampleVariable);

	// FUunction arg cannot be used by a function if there are no parameter provided within the function.
	function noParams(){
		let params = "No parameter"

		console.log(params);
	}

	noParams("With parameters!");

	// Check divisibility
	function checkDivBy8(num){
		let modulo=num%8
		console.log("The remainder of " + num + " divided by 8 is: " + modulo);

		let isDivBy8 = modulo === 0;
		console.log("Is " + num + " div by 8?");
		console.log(isDivBy8);
	}
		checkDivBy8(8);
		checkDivBy8(17);

	//Functions as Arguments

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed");
		}

		function argumentFunction2(){
			console.log("This function was passed as an argument from the 2nd argument");
		}

		function invokeFunction(argFunction){
			argFunction();
		}

		invokeFunction(argumentFunction);
		;

		invokeFunction(argumentFunction2);

		// using multiple parameters

		function createFullname(firstName, middleName, lastName){
			console.log("This is a firstName: " + firstName);
			console.log("This is middleName: " + middleName);
			console.log("This is a lastName: " + lastName);
		}

		createFullname("Juan", "Dela", "Cruz");

		createFullname ("Juan", "Dela", "Cruz", "Jr.");

		createFullname("Juan", undefined, "Dela Cruz")

		// using var as arg

		let firstName = "john";
		let middleName = "Doe"
		let lastName = "Smith"

		createFullname(firstName, middleName, lastName);


	 // Return Statement

	 function returnFullName(firstName, middleName, lastName){
	 	console.log(firstName + " " + middleName + " " + lastName)
	 } 

	 returnFullName("Ada", "None", "Lovelace");

	 function returnName(firstName, middleName, lastName){
	 	return firstName + " " + middleName + " " + lastName;
	 }

	 console.log(returnName("John", "Doe", "Smith"));

	 let fullname = returnName("John", "Doe", "Smith");
	 console.log("This is the console.log from fullname variable");
	 console.log(fullname);

	 function printPlayerInfo(userName, level, job){
	 	console.log("Username: " + userName);
	 	console.log("Level: " + level);
	 	console.log("Job: " + job);

	 	return userName + " " + level + " " + job;
	 }

	 printPlayerInfo("Knight_white", 95, "Paladin");

	 let user1= printPlayerInfo("Knight_white", 95, "Paladin");
	 console.log(user1); 